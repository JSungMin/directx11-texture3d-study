// 픽셀 셰이더를 통과한 픽셀당 색 데이터입니다.
struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float2 texcoord    : TEXCOORD0;
};

struct Pixel
{
	float4 color    : SV_Target;
};

Texture2D txDiffuse : register(t0);
SamplerState sampleState : register(s0);


// (보간된) 색 데이터에 대한 통과 함수입니다.
Pixel main(PixelShaderInput input) : SV_TARGET
{

	Pixel Out;
	Out.color = txDiffuse.Sample(sampleState, input.texcoord);
	return Out;
}
